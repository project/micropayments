Micropayments
================================================================================

Summary
--------------------------------------------------------------------------------

The Micropayments module makes it possible to accept micropayments through the
micropayments.dk service.


Requirements
--------------------------------------------------------------------------------

The Micropayments module requires the latest version of the PHP OpenID library.

You can download it from: http://openidenabled.com/ (current version is 2.1.3)


Installation
--------------------------------------------------------------------------------

1. Copy the micropayments folder to sites/all/modules or to a site-specific
   modules folder.

2. Extract the PHP OpenID library to micropayments/php-openid.

3. Go to Administer > Site building > Modules and enable the Micropayments
   module.


Configuration
--------------------------------------------------------------------------------

1. Go to Administer > Content > Taxonomy and enable at least one content type
   in the Micropayments vocabulary.

2. (Optional) Go to Administer > Settings > Micropayments and select which
   OpenID storage type you want to use.


Support
--------------------------------------------------------------------------------

Please post bug reports and feature requests in the issue queue:

  http://drupal.org/project/micropayments


Credits
--------------------------------------------------------------------------------

Author: Morten Wulff <wulff@ratatosk.net>


