<?php

/**
 * Require the base class file.
 */
require_once "Auth/OpenID/DatabaseConnection.php";

class Auth_OpenID_DrupalDatabaseConnection extends Auth_OpenID_DatabaseConnection {
    var $autoCommit = FALSE;

    /**
     * Sets auto-commit mode on this database connection.
     *
     * @param bool $mode True if auto-commit is to be used; false if
     * not.
     */
    function autoCommit($mode)
    {
      $this->autoCommit = $mode;
    }

    /**
     * Run an SQL query with the specified parameters, if any.
     *
     * @param string $sql An SQL string with placeholders.  The
     * placeholders are assumed to be specific to the database engine
     * for this connection.
     *
     * @param array $params An array of parameters to insert into the
     * SQL string using this connection's escaping mechanism.
     *
     * @return mixed $result The result of calling this connection's
     * internal query function.  The type of result depends on the
     * underlying database engine.  This method is usually used when
     * the result of a query is not important, like a DDL query.
     */
    function query($sql, $params = array())
    {
      $sql = str_replace(array('!', '?'), "'%s'", $sql);
      $result = db_query($sql, $params);
    }

    /**
     * Starts a transaction on this connection, if supported.
     */
    function begin()
    {
      $foo = 42;
    }

    /**
     * Commits a transaction on this connection, if supported.
     */
    function commit()
    {
      $foo = 42;
    }

    /**
     * Performs a rollback on this connection, if supported.
     */
    function rollback()
    {
      $foo = 42;
    }

    /**
     * Run an SQL query and return the first column of the first row
     * of the result set, if any.
     *
     * @param string $sql An SQL string with placeholders.  The
     * placeholders are assumed to be specific to the database engine
     * for this connection.
     *
     * @param array $params An array of parameters to insert into the
     * SQL string using this connection's escaping mechanism.
     *
     * @return mixed $result The value of the first column of the
     * first row of the result set.  False if no such result was
     * found.
     */
    function getOne($sql, $params = array())
    {
      $foo = 42;
    }

    /**
     * Run an SQL query and return the first row of the result set, if
     * any.
     *
     * @param string $sql An SQL string with placeholders.  The
     * placeholders are assumed to be specific to the database engine
     * for this connection.
     *
     * @param array $params An array of parameters to insert into the
     * SQL string using this connection's escaping mechanism.
     *
     * @return array $result The first row of the result set, if any,
     * keyed on column name.  False if no such result was found.
     */
    function getRow($sql, $params = array())
    {
      $sql = str_replace('?', "'%s'", $sql);
      $result = db_query($sql, $params);

      return db_fetch_array($result);
    }

    /**
     * Run an SQL query with the specified parameters, if any.
     *
     * @param string $sql An SQL string with placeholders.  The
     * placeholders are assumed to be specific to the database engine
     * for this connection.
     *
     * @param array $params An array of parameters to insert into the
     * SQL string using this connection's escaping mechanism.
     *
     * @return array $result An array of arrays representing the
     * result of the query; each array is keyed on column name.
     */
    function getAll($sql, $params = array())
    {
      $sql = str_replace('?', "'%s'", $sql);
      $result = db_query($sql, $params);

      $rows = array();
      while ($row = db_fetch_array($result)) {
        $rows[] = $row;
      }

      return $rows;
    }
}
