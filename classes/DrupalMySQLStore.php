<?php

/**
 * Require the base class file.
 */
require_once "Auth/OpenID/MySQLStore.php";

class Auth_OpenID_DrupalMySQLStore extends Auth_OpenID_MySQLStore {
  /**
   * Returns true if $value constitutes a database error; returns
   * false otherwise.
   */
  function isError($value) {
    // FIXME: modify DrupalDatabaseConnection and this function to enable error handling.
    return FALSE;
  }

  /**
   * @access private
   * @see http://lists.openidenabled.com/pipermail/dev/2007-September/000850.html
   */
  function blobEncode($str) {
    return $str;
  }
}
