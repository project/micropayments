<?php

/**
 * Implementation of Micropayments Payment Request.
 */

/**
 * Import message and extension internals.
 */
require_once 'Auth/OpenID/Message.php';
require_once 'Auth/OpenID/Extension.php';

// The data fields that are listed in the mppr spec
global $Auth_OpenID_mppr_fields;
$Auth_OpenID_mppr_fields = array(
  'productId' => 'Product ID',
  'referenceId' => 'Reference ID',
  'referenceName' => 'Reference Name',
  'hashChallenge' => 'Hash challenge',
  'transactionId' => 'Transaction ID',
  'transactionType' => 'Transaction type'
);

define('Auth_OpenID_MPPR_NS_URI_1_0', 'http://schemas.micropayments.dk/2009/payment/1.0');

define('Auth_OpenID_MPPR_NS_URI', Auth_OpenID_MPPR_NS_URI_1_0);

Auth_OpenID_registerNamespaceAlias(Auth_OpenID_MPPR_NS_URI_1_0, 'mppr');

/**
 * Does the given endpoint advertise support for payment
 * request?
 *
 * $endpoint: The endpoint object as returned by OpenID discovery.
 * returns whether an mppr type was advertised by the endpoint
 */
function Auth_OpenID_supportPaymentReg(&$endpoint) {
  return ($endpoint->usesExtension(Auth_OpenID_MPPR_NS_URI_1_0));
}

/**
 * A base class for classes dealing with Payment Request protocol
 * messages.
 */
class Auth_OpenID_PaymentBase extends Auth_OpenID_Extension {
  function _getPaymentNS(&$message) {
    $alias = null;
    $found_ns_uri = null;

    // See if there exists an alias for one of the two defined
    // simple registration types.
    foreach (array(Auth_OpenID_MPPR_NS_URI_1_0) as $sreg_ns_uri) {
      $alias = $message->namespaces->getAlias($sreg_ns_uri);
      if ($alias !== null) {
          $found_ns_uri = $sreg_ns_uri;
          break;
      }
    }

    if ($alias === null) {
      // There is no alias for either of the types, so try to
      // add one. We default to using the modern value (1.1)
      $found_ns_uri = Auth_OpenID_MPPR_NS_URI_1_0;
      if ($message->namespaces->addAlias(Auth_OpenID_MPPR_NS_URI_1_0, 'mppr') === null) {
        // An alias for the string 'mppr' already exists, but
        // it's defined for something other than simple
        // registration
        return null;
      }
    }

    return $found_ns_uri;
  }
}

/**
 * An object to hold the state of a payment request.
 *
 * paymentRequestArgs: A list of the required fields for a payment request.
 *
 */
class Auth_OpenID_PaymentRequest extends Auth_OpenID_PaymentBase {
  var $ns_alias = 'mppr';

  /**
   * Initialize an empty simple registration request.
   */
  function build($paymentRequestArgs, $policy_url=null, $sreg_ns_uri=Auth_OpenID_MPPR_NS_URI, $cls='Auth_OpenID_PaymentRequest') {
    $obj = new $cls();

    $obj->paymentArgs = $paymentRequestArgs;
    $obj->policy_url = $policy_url;
    $obj->ns_uri = $sreg_ns_uri;

    return $obj;
  }

  function parseExtensionArgs($args, $strict=false) {
    $this->policy_url = Auth_OpenID::arrayGet($args, 'policy_url');

    return true;
  }

  /**
   * Get a dictionary of unqualified mppr arguments
   * representing this request.
   *
   * This method is essentially the inverse of
   * C{L{parseExtensionArgs}}. This method serializes the mppr
   * fields.
   */
  function getExtensionArgs() {
    $args = array();

    foreach ($this->paymentArgs as $key => $value) {
      $args[$key] = $value;
    }

    return $args;
  }
}

/**
 * Represents the data returned in a mppr response
 * inside of an OpenID C{id_res} response. This object will be created
 * by the OpenID server, added to the C{id_res} response object, and
 * then extracted from the C{id_res} message by the Consumer.
 *
 * @package OpenID
 */
class Auth_OpenID_PaymentResponse extends Auth_OpenID_PaymentBase {
  var $ns_alias = 'mppr';

  function Auth_OpenID_PaymentResponse($data=null, $sreg_ns_uri=Auth_OpenID_MPPR_NS_URI) {
    if ($data === null) {
        $this->data = array();
    }
    else {
        $this->data = $data;
    }

    $this->ns_uri = $sreg_ns_uri;
  }

  /**
   * Create a C{L{PaymentResponse}} object from a successful OpenID
   * library response
   * (C{L{openid.consumer.consumer.SuccessResponse}}) response
   * message
   *
   * success_response: A SuccessResponse from consumer.complete()
   *
   * signed_only: Whether to process only data that was
   * signed in the id_res message from the server.
   *
   * Returns a mppr response containing the data that
   * was supplied with the C{id_res} response.
   */
  function fromSuccessResponse(&$success_response, $signed_only=true) {
    global $Auth_OpenID_mppr_fields;

    $obj = new Auth_OpenID_PaymentResponse();
    $obj->ns_uri = $obj->_getPaymentNS($success_response->message);

    if ($signed_only) {
      $args = $success_response->getSignedNS($obj->ns_uri);
    }
    else {
      $args = $success_response->message->getArgs($obj->ns_uri);
    }

    if ($args === null || Auth_OpenID::isFailure($args)) {
      return null;
    }

    foreach ($Auth_OpenID_mppr_fields as $field_name => $desc) {
      if (in_array($field_name, array_keys($args))) {
        $obj->data[$field_name] = $args[$field_name];
      }
    }

    return $obj;
  }

  function getExtensionArgs() {
    return $this->data;
  }

  // Read-only dictionary interface
  function get($field_name, $default=null) {
    if (!Auth_OpenID_checkFieldName($field_name)) {
      return null;
    }

    return Auth_OpenID::arrayGet($this->data, $field_name, $default);
  }

  function contents() {
    return $this->data;
  }
}
