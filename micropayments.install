<?php

/**
 * @file
 * Install file for the micropayments module.
 */

// FIXME: check for presence of php-openid
// FIXME: add micropayments_override for node-specific currency and price overrides

/**
 * Implementation of hook_requirements().
 */
function micropayments_requirements($phase) {
  if ($phase == 'install') {
    $path = drupal_get_path('module', 'micropayments') .'/php-openid/';

    if (!file_exists($path)) {
      return array(
        'php_openid' => array(
          'title' => t('OpenID Library'),
          'description' => t("Couldn't locate PHP OpenID library at %path. Please see the included README.txt for information on where to get this library.", array('%path' => $path)),
          'severity' => REQUIREMENT_ERROR,
        ),
      );
    }
    else {
      set_include_path(get_include_path() . PATH_SEPARATOR . $path);
      require_once 'Auth/OpenID.php';

      $version = explode('.', Auth_OpenID_VERSION);

      if (!($version[0] >= 2 && $version[1] >= 1)) {
        return array(
          'php_openid' => array(
            'title' => t('OpenID Library'),
            'value' => Auth_OpenID_VERSION,
            'description' => t('This module requires at least version 2.1.x of the PHP OpenID library'),
            'severity' => REQUIREMENT_ERROR,
          ),
        );
      }
    }
  }
}

/**
 * Implementation of hook_install().
 */
function micropayments_install() {
  drupal_install_schema('micropayments');
  db_query("UPDATE {system} SET weight = 1 WHERE name = 'micropayments'");
}

/**
 * Implementation of hook_enable().
 */
function micropayments_enable() {
  $vocabulary = taxonomy_vocabulary_load(variable_get('micropayments_vocabulary', 0));
  if (!$vocabulary) {
    $vocabulary = array(
      'name' => t('Micropayments'),
      'description' => t('The terms in this vocabulary should match the products you have defined in your micropayments.dk account.'),
      'multiple' => 0,
      'required' => 0,
      'hierarchy' => 1,
      'relations' => 0,
      'module' => 'micropayments',
      'weight' => -10,
    );
    taxonomy_save_vocabulary($vocabulary);

    variable_set('micropayments_vocabulary', $vocabulary['vid']);
  }
}

/**
 * Implementation of hook_uninstall().
 */
function micropayments_uninstall() {
  drupal_load('module', 'taxonomy');

  $vid = variable_get('micropayments_vocabulary', 0);
  taxonomy_del_vocabulary($vid);

  drupal_uninstall_schema('micropayments');

  variable_del('micropayments_vocabulary');
  variable_del('micropayments_store');
}

/**
 * Implementation of hook_schema().
 */
function micropayments_schema() {
  $schema['micropayments_product'] = array(
    'description' => 'Stores information about the available micropayment products.',
    'fields' => array(
      'tid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {term_data}.tid of the product term.',
      ),
      'product_id' => array(
        'description' => 'Micropayment product ID in the format xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx.',
        'type' => 'varchar',
        'length' => 36,
        'not null' => TRUE,
        'default' => '',
      ),
      'reference_name' => array(
        'description' => 'Micropayment reference name. Max 400 unicode characters.',
        'type' => 'text',
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('tid'),
  );

  $schema['micropayments_openid_nonce'] = array(
    'description' => 'MySQLStore table',
    'fields' => array(
      'server_url' => array(
        'type' => 'varchar',
        'length' => 2047,
        'not null' => TRUE,
        'default' => '',
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'salt' => array(
        'type' => 'char',
        'length' => 40,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array(array('server_url', 255), 'timestamp', 'salt'),
  );

  $schema['micropayments_openid_assoc'] = array(
    'description' => 'MySQLStore table',
    'fields' => array(
      'server_url' => array(
        'type' => 'varchar',
        'length' => 2047,
        'not null' => TRUE,
        'default' => '',
      ),
      'handle' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'secret' => array(
        'type' => 'blob',
        'not null' => TRUE,
      ),
      'issued' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'lifetime' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'assoc_type' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ),
      'salt' => array(
        'type' => 'char',
        'length' => 40,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    // http://bugs.mysql.com/bug.php?id=4541
    'primary key' => array(array('server_url', 128), array('handle',128)),
  );

  $schema['micropayments_transaction'] = array(
    'description' => 'The base table for micropayments.',
    'fields' => array(
      'uid' => array(
        'description' => 'User ID of the user making the payment.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'nid' => array(
        'description' => 'The node the user has bought access to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'product_id' => array(
        'description' => 'Micropayment product ID in the format xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx.',
        'type' => 'varchar',
        'length' => 36,
        'not null' => TRUE,
        'default' => '',
      ),
      'transaction_id' => array(
        'description' => 'Micropayment transaction ID in the format xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx.',
        'type' => 'varchar',
        'length' => 36,
        'not null' => TRUE,
        'default' => '',
      ),
      'reference_id' => array(
        'description' => 'Micropayment reference ID. Max 400 unicode characters.',
        'type' => 'text',
        'not null' => TRUE,
        'default' => '',
      ),
      'reference_name' => array(
        'description' => 'Micropayment reference name. Max 400 unicode characters.',
        'type' => 'text',
        'not null' => TRUE,
        'default' => '',
      ),
      'price_override' => array(
        'description' => 'Override the default price of a product.',
        'type' => 'float',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'hash_challenge' => array(
        'description' => 'Micropayment hash challenge.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('uid', 'nid'),
  );

  return $schema;
}
